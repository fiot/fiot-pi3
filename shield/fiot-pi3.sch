EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:fiot-pi3-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L +3V3 #PWR01
U 1 1 5AA14457
P 2050 1250
F 0 "#PWR01" H 2050 1100 50  0001 C CNN
F 1 "+3V3" H 2050 1390 50  0000 C CNN
F 2 "" H 2050 1250 50  0001 C CNN
F 3 "" H 2050 1250 50  0001 C CNN
	1    2050 1250
	1    0    0    -1  
$EndComp
$Comp
L Conn_02x20_Odd_Even J8
U 1 1 5AA6F7C8
P 2350 2150
F 0 "J8" H 2400 3150 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 2400 1050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Angled_2x20_Pitch2.54mm" H 2350 2150 50  0001 C CNN
F 3 "" H 2350 2150 50  0001 C CNN
	1    2350 2150
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J1
U 1 1 5AA70515
P 1000 1100
F 0 "J1" H 1000 1300 50  0000 C CNN
F 1 "Conn_01x03" H 1000 900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 1000 1100 50  0001 C CNN
F 3 "" H 1000 1100 50  0001 C CNN
	1    1000 1100
	-1   0    0    1   
$EndComp
$Comp
L R R3
U 1 1 5AA7080F
P 1850 1400
F 0 "R3" V 1930 1400 50  0000 C CNN
F 1 "R" V 1850 1400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1780 1400 50  0001 C CNN
F 3 "" H 1850 1400 50  0001 C CNN
	1    1850 1400
	1    0    0    -1  
$EndComp
$Comp
L Conn_01x03 J7
U 1 1 5AA70999
P 1450 2250
F 0 "J7" H 1450 2450 50  0000 C CNN
F 1 "Conn_01x03" H 1450 2050 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 1450 2250 50  0001 C CNN
F 3 "" H 1450 2250 50  0001 C CNN
	1    1450 2250
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 2250 1950 2250
Wire Wire Line
	1850 1250 2150 1250
Wire Wire Line
	1650 1800 1650 2150
Wire Wire Line
	1650 2450 2150 2450
Wire Wire Line
	1650 2550 1650 2350
Wire Wire Line
	1950 2250 1950 2150
Wire Wire Line
	1950 2150 2150 2150
Wire Wire Line
	800  2550 1650 2550
Wire Wire Line
	1250 2550 1250 1900
Connection ~ 1650 2450
$Comp
L R R1
U 1 1 5AA711D8
P 1800 2100
F 0 "R1" V 1880 2100 50  0000 C CNN
F 1 "R" V 1800 2100 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1730 2100 50  0001 C CNN
F 3 "" H 1800 2100 50  0001 C CNN
	1    1800 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 1950 2000 1950
Wire Wire Line
	2000 1950 2000 2050
Wire Wire Line
	2000 2050 2150 2050
Connection ~ 1800 1950
Connection ~ 1800 2250
Connection ~ 1850 1250
Connection ~ 2050 1250
Text Notes 1900 800  0    60   ~ 0
FiOT Pi3 Shield ver 0.1\n
$Comp
L Conn_01x04 J2
U 1 1 5AA706D4
P 1050 1800
F 0 "J2" H 1050 2000 50  0000 C CNN
F 1 "Conn_01x04" H 1050 1500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04_Pitch2.00mm" H 1050 1800 50  0001 C CNN
F 3 "" H 1050 1800 50  0001 C CNN
	1    1050 1800
	-1   0    0    1   
$EndComp
Wire Wire Line
	1650 1800 1250 1800
Connection ~ 1650 1950
Wire Wire Line
	800  2550 800  1400
Wire Wire Line
	800  1400 1200 1400
Wire Wire Line
	1200 1400 1200 1200
Connection ~ 1250 2550
Wire Wire Line
	1200 1000 1850 1000
Wire Wire Line
	1850 1000 1850 1250
Connection ~ 1300 1000
$Comp
L Conn_01x03 J3
U 1 1 5AA71CC2
P 1000 3000
F 0 "J3" H 1000 3200 50  0000 C CNN
F 1 "Conn_01x03" H 1000 2800 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x03_Pitch2.54mm" H 1000 3000 50  0001 C CNN
F 3 "" H 1000 3000 50  0001 C CNN
	1    1000 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	1200 2700 1200 2900
Wire Wire Line
	1200 3100 1800 3100
Wire Wire Line
	1800 3100 1800 3150
Wire Wire Line
	1800 3150 2150 3150
Wire Wire Line
	1200 3000 1850 3000
Wire Wire Line
	1850 3000 1850 2650
Wire Wire Line
	1850 2650 2150 2650
$Comp
L R R2
U 1 1 5AA71E38
P 1500 2850
F 0 "R2" V 1580 2850 50  0000 C CNN
F 1 "R" V 1500 2850 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 1430 2850 50  0001 C CNN
F 3 "" H 1500 2850 50  0001 C CNN
	1    1500 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	650  2700 1500 2700
Connection ~ 1200 2750
Connection ~ 1500 3000
Connection ~ 2050 2150
$Comp
L GND #PWR02
U 1 1 5AA7272F
P 1800 3150
F 0 "#PWR02" H 1800 2900 50  0001 C CNN
F 1 "GND" H 1800 3000 50  0000 C CNN
F 2 "" H 1800 3150 50  0001 C CNN
F 3 "" H 1800 3150 50  0001 C CNN
	1    1800 3150
	1    0    0    -1  
$EndComp
Connection ~ 1800 3150
$Comp
L +3V3 #PWR03
U 1 1 5AA72B2E
P 2000 1950
F 0 "#PWR03" H 2000 1800 50  0001 C CNN
F 1 "+3V3" H 2000 2090 50  0000 C CNN
F 2 "" H 2000 1950 50  0001 C CNN
F 3 "" H 2000 1950 50  0001 C CNN
	1    2000 1950
	1    0    0    -1  
$EndComp
Connection ~ 2000 1950
$Comp
L GND #PWR04
U 1 1 5AA733EB
P 2000 2450
F 0 "#PWR04" H 2000 2200 50  0001 C CNN
F 1 "GND" H 2000 2300 50  0000 C CNN
F 2 "" H 2000 2450 50  0001 C CNN
F 3 "" H 2000 2450 50  0001 C CNN
	1    2000 2450
	1    0    0    -1  
$EndComp
Connection ~ 2000 2450
Wire Wire Line
	1600 1700 1250 1700
Wire Wire Line
	1600 1100 1600 1700
Wire Wire Line
	1250 1600 2000 1600
Wire Wire Line
	1850 1600 1850 1550
Connection ~ 1600 1600
Wire Wire Line
	2000 1600 2000 1550
Wire Wire Line
	2000 1550 2150 1550
Connection ~ 1850 1600
Wire Wire Line
	1600 1100 1200 1100
Wire Wire Line
	1300 1000 1300 800 
Wire Wire Line
	1300 800  650  800 
Wire Wire Line
	650  800  650  2700
Connection ~ 1200 2700
$EndSCHEMATC
